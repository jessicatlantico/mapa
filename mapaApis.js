/*switch variables*/
var marker;

function updatePos(lat, lng) {
  var api_key_weather = "05368a30f30aa3de0d153301a9ac52f6";
  var temp = "https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lng + "&units=metric&appid=" + api_key_weather;
  var forecast = "https://api.openweathermap.org/data/2.5/forecast?lat=" + lat + "&lon=" + lng + "&units=metric&appid=" + api_key_weather;
  var jqxhr = $.getJSON(temp)
    .done(function(data) {
      $("#temperatura").html(data.main.temp + " ℃");
      var iconcode = data.weather[0].icon;
      var iconurl = "http://openweathermap.org/img/w/" + iconcode + ".png"
      $("#icon").attr('src', iconurl);
      $("#lugar").html(data.name);

    })
    .fail(function(data) {
      $("#temperatura").html("Lo siento! no es posible conectar con nuestros servicio de climas");
      console.log("error conectando a API openweathermap.org");
    })
    .always(function() {
      //cuando termina, no lo utilizamos
    });

  var jqxhr2 = $.getJSON(forecast)
    .done(function(data) {
      var completeForecast = "";
      for (var i = 0; i <= 8; i++) {
        completeForecast += data.list[i].dt_txt.substring(12) + "-" +
          data.list[i].main.temp +
          "<img src=http://openweathermap.org/img/w/" + data.list[i].weather[0].icon + ".png>";

      }
      $("#forecast").html(completeForecast);



    })
    .fail(function(data) {
      $("#forecast").html("Lo siento! no es posible conectar con nuestros servicio de climas");
      console.log("error conectando a API openweathermap.org");
    })
    .always(function() {
      //cuando termina, no lo utilizamos
    });

}
/* Google Maps API*/

var map;

function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {
      lat: 43.462041,
      lng: -3.809972
    },
    zoom: 5
  });
  marker = new google.maps.Marker({
    position: map.getCenter(),
    icon: {
      path: google.maps.SymbolPath.MARKER,
      scale: 10
    },
    draggable: true,
    map: map
  });
  google.maps.event.addListener(marker, 'dragend', function(event) {
    var lat = marker.getPosition().lat();
    var lng = marker.getPosition().lng();
    updatePos(lat, lng);

  });

}
$(document).ready(function() {
  var lat = 43.462041;
  var lng = -3.809972;
  updatePos(lat, lng);
});
